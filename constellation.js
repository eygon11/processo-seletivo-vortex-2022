/*
 Criar uma matriz quadrada, com '0' nas posições da diagonal
 e números aleatórios entre 0 e 1 nas demais posições 
 */

//Parte 1: Entrada dos dados
do {

    var entrada = prompt("Hey input the constellation info following this structure: '{5, 3, 2}'");
    entrada = entrada.replace(/[{}]/g, "");
    var [starsQnt, firstStar, secondStar] = entrada.split(", ").map(Number);

  } while (starsQnt < 4 || starsQnt > 8);
  
  //Parte 2:  Criação da Matriz
  let totalArray = [];
  
  function createMatrix(rowsQnt) {
    for (let row = 0; row < rowsQnt; row++) {
      let initialArray = [];
      for (let column = 0; column < rowsQnt; column++) {
        row === column
          ? initialArray.push(0)
          : initialArray.push(Math.floor(Math.random() * 2));
      }
      totalArray[row] = [...initialArray];
    }
  
    for (let row = 0; row < rowsQnt; row++) {
      for (let column = 0; column < rowsQnt; column++) {
        if (totalArray[row][column] === 1) totalArray[column][row] = 1;
      }
    }
    return totalArray;
  }
  
  createMatrix(starsQnt);
  
  //Parte 3: Checagem das conexões
  if (
    totalArray[firstStar][secondStar] == 1 &&
    totalArray[secondStar][firstStar] == 1
  ) {
    console.log(printMatrix(totalArray) + ", Há ligação");
  } else {
    console.log(printMatrix(totalArray) + ", Não há ligação");
  }
  
  function printMatrix(matrix) {
    let formatedMatrix = "[\n";
  
    for (let row = 0; row < starsQnt; row++) {
      formatedMatrix = `${formatedMatrix}\n[${matrix[row]}]\n`;
    }
    formatedMatrix = `${formatedMatrix}\n]`;
    return formatedMatrix;
  }