//declaração de variaveis
var baralho = [];
var cartasMesa = [];
var bot1 = [];
var bot2 = [];
var valores = [];
var valores2 = [];

var royalFlush = [];
var straightFlush = [];
let straight = [];
var quadra = [];
var cartaDeferente = true;
var fullHouse = [];
var fullHouseAux = []
var fullHousebeckup
var flush = [];
var trio = []
var doisPares = [];
var segundoPar = [];
var par = [];
var highestCard = [];

var qntCopas = 0;
var qntEspada = 0;
var qntOuro = 0;
var qntPaus = 0;

var bestSuit;
var resultado;
var cartaParaComparacao;
var valorMaiorFinal

var deckFinal = [];

var bot1Combo = []
var bot1DeckFinal = []
var bot1highestCard = ""

var bot2Combo = []
var bot2DeckFinal = []
var bot2highestCard = ""
var valorDoCombo = 0


function createDeck() {
  const suit = ["C", "E", "O", "P"];
  const value = [
    "-2",
    "-3",
    "-4",
    "-5",
    "-6",
    "-7",
    "-8",
    "-9",
    "-10",
    "-J",
    "-Q",
    "-K",
    "-A"
  ];

  for (let i = 0; i < 4; i++) {
    for (let j = 0; j <= 12; j++) {
      let cartaDaVez = suit[i].concat(value[j]);
      baralho.push(cartaDaVez);
    }
  }
}

function shuffle(roll, qntCards = 52) {
  for (let i = 0; i < qntCards; i++) {
    var nRadon = Math.floor(Math.random() * qntCards);
    roll.splice(nRadon, 0, roll.splice(i, 1)[0]);
  }
}

function cardDealer() {
  for (let i = 0; i < 2; i++) {
    bot1.push(baralho.pop());
    bot2.push(baralho.pop());
  }

  for (let i = 0; i < 5; i++) {
    cartasMesa.push(baralho.pop());
  }
}

function formatting(handB1, handB2) {
  for (let i = 0; i < 5; i++) {
    bot1.push(cartasMesa[i]);
    bot2.push(cartasMesa[i]);
  }

  for (let i = 0; i < 7; i++) {
    var teste = handB1.pop();
    varlores = valores.push(teste.split("-"));
  }

  for (let i = 0; i < 7; i++) {
    var teste2 = handB2.pop();
    varlores2 = valores2.push(teste2.split("-"));
  }
}

function beastValue(inputArray) {
  for (let i = 0; i < 7; i++) {
    if (inputArray[i][0] === "C") {
      qntCopas++;
    } else if (inputArray[i][0] === "E") {
      qntEspada++;
    } else if (inputArray[i][0] === "O") {
      qntOuro++;
    } else if (inputArray[i][0] === "P") {
      qntPaus++;
    }
  }

  if (qntCopas >= 5) {
    bestSuit = "C";
  } else if (qntEspada >= 5) {
    bestSuit = "E";
  } else if (qntOuro >= 5) {
    bestSuit = "O";
  } else if (qntPaus >= 5) {
    bestSuit = "P";
  }
}

function combos(deck) {

  if (qntCopas >= 5 || qntEspada >= 5 || qntOuro >= 5 || qntPaus >= 5) {
    //royalFlush
    const arrayRoyalFlush = ["10", "J", "Q", "K", "A"];

    for (let i = 0; i < 5; i++) {
      for (let j = 0; j < 7; j++) {
        if (deck[j][0] === bestSuit) {
          if (deck[j][1] === arrayRoyalFlush[i]) {
            royalFlush.push(deck[j][0].concat(deck[j][1]));
          }
        }
      }
    }
    if (royalFlush.length === 5) {
      resultado = "royalFlush"
      deckFinal = royalFlush;
      return resultado, deckFinal;
    }

    //arrayStraightFlush
    const arrayStraightFlush = [
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "J",
      "Q",
      "K"
    ];

    let isStraightFlush = false;
    let straightFlush = [];
    var newDeck = deck
      .filter((value) => value[0] === bestSuit)
      .map((value) => value[1]);

    for (let index = 0; index <= 8; index++) {
      let arrayToCompare = arrayStraightFlush.slice(index, 5 + index);
      var auxArray = [];

      newDeck.map((card) => {
        if (arrayToCompare.includes(card)) auxArray.push(card);
        return straightFlush;
      });
      if (auxArray.length === 5) {
        resultado = "straightFlush"
        deckFinal = straightFlush
        return resultado, deckFinal;
        straightFlush = auxArray;
      }
    }
  }

  //quadra
  for (let i = 0; i < 2; i++) {
    for (let j = 0; j < 7; j++) {
      if (quadra.length < 4) {
        if (deck[i][1] === deck[j][1]) {
          quadra.push(deck[j][0].concat(deck[i][1]));
        }
      }
    }
    if (quadra.length < 4) {
      quadra = [];
    }
  }
  if (quadra.length === 4) {
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 7; j++) {
        if (quadra[1][1] !== deck[j][1] && quadra.length === 4) {
          quadra.push(deck[i][0].concat(deck[j][1]));
        }
      }
    }
  }
  if (quadra.length === 5) {
    resultado = "quadra";
    deckFinal = quadra
    return resultado, deckFinal;
  }

  //fullHouse
  var trioValue = ""
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 7; j++) {
      if (deck[i][1] === deck[j][1]) {
        fullHouse.push(deck[j][0].concat(deck[j][1]));
        trioValue = deck[j][1];
      }
    }
    if (fullHouse.length < 3) {
      fullHouse = [];
    }
    if (fullHouse.length === 3) {
      fullHouseAux = fullHouse
      break
    }
  }
  //o fullHouse ta pegando os 3 primeiros valores falta a dupla
  if (fullHouse.length === 3) {
    fullHouseAux = fullHouse
    for (let i = 0; i < 7; i++) {
      for (let j = i; j < 6; j++) {

        if (deck[i][1] != trioValue && deck[i][1] == deck[j + 1][1]) {
          fullHouse.push(deck[j][0].concat(deck[j][1]));
          fullHouse.push(deck[i][0].concat(deck[i][1]));
        }
      }
    }
  }
  if (fullHouse.length === 5) {
    resultado = "fullHouse";
    deckFinal = fullHouse
    return resultado, deckFinal;
  }

  //flush
  if (qntCopas >= 5 || qntEspada >= 5 || qntOuro >= 5 || qntPaus >= 5) {
    for (let i = 0; i < 5; i++) {
      //concatena a carta com o seu respectivo naipe
      flush.push(bestSuit.concat(newDeck[i]))
    }
    if (flush.length === 5) {
      resultado = "flush"
      deckFinal = flush
      return resultado, deckFinal;
    }
  }

  //straight
  const arrayStraight = [
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "J",
    "Q",
    "K"
  ];

  for (let index = 0; index <= 8; index++) {
    let arrayToCompare = arrayStraight.slice(index, 5 + index);
    //gera as possiveis entradas do straight
    let auxArray = [];
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 11; j++) {
        if (arrayToCompare[j] == (deck[i][1])) {
          //verifica se a carta existe no arrayToCompare
          auxArray.push(deck[i][0].concat(deck[i][1]));
          arrayToCompare.splice(arrayToCompare.indexOf(deck[i][1]), 1);
        }
      }
    }
    if (auxArray.length === 5) {
      resultado = "straight";
      deckFinal = auxArray
      return resultado, deckFinal;
    }
  }

  //trio
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 7; j++) {
      //procura o trio de cartas com o valor igual
      if (deck[i][1] === deck[j][1]) {
        trio.push(deck[j][0].concat(deck[j][1]));
        trioValue = deck[j][1];
      }
    }
    if (trio.length < 3) {
      trio = [];
    }
    if (trio.length === 3) {
      break
    }
  }
  if (trio.length === 3) {
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        //adiciona outras duas catas quaisquer
        if (trioValue != deck[j][1] && deck[i][1] === deck[j][1]) {
          trio.push(deck[i][0].concat(deck[i][1]));

        }
        if (trio.length === 5) {
          break
        }

      }
    }
  }
  if (trio.length === 5) {
    resultado = "trio";
    deckFinal = trio
    return resultado, deckFinal;
  }

  //two pair
  doisParesValue = "";
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 7; j++) {
      //procura o primeiro par
      if (deck[i][1] === deck[j][1]) {
        doisPares.push(deck[j][0].concat(deck[j][1]));
        doisParesValue = deck[j][1];
      }
    }
    if (doisPares.length < 2) {
      doisPares = [];
    }
    if (doisPares.length === 2) {
      break
    }
  }
  if (doisPares.length == 2) {
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        //procura o segundo par 
        if (deck[i][1] === deck[j][1] && deck[i][1] !== doisParesValue) {
          segundoPar.push(deck[j][0].concat(deck[j][1]));
        }
      }
      if (segundoPar.length < 2) {
        segundoPar = [];
      }
      if (segundoPar.length === 2) {
        break
      }
    }
  }
  if (segundoPar.length === 2) {
    //Concatena os dois pares
    doisPares = [...doisPares, ...segundoPar]
  }
  if (doisPares.length === 4) {
    //adiciona uma ultima carta qualquer
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 7; j++) {
        if (doisPares[1][1] !== deck[j][1] && doisPares.length <= 4 && doisParesValue !== deck[j][1]) {
          doisPares.push(deck[i][0].concat(deck[j][1]));

        }
      }
    }
    resultado = "doisPares"
    deckFinal = doisPares
    return resultado, deckFinal;
  }
  //par
  parValue = "";
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 7; j++) {
      if (deck[i][1] === deck[j][1]) {
        par.push(deck[j][0].concat(deck[j][1]));
        parValue = deck[j][1];

      }
    }
    if (par.length < 2) {
      par = [];
    }
    if (par.length === 2) {
      break
    }
  }
  if (par.length === 2) {
    //adiciona outras três cartas quaisquer
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 7; j++) {
        if (par[1][1] !== deck[j][1] && par.length <= 4 && parValue !== deck[j][1]) {
          par.push(deck[i][0].concat(deck[j][1]));

        }
      }
    }
    resultado = "par";
    deckFinal = par;
    return resultado, deckFinal
  }

  //carta mais alta
  const arrayHighestCard = [
    "A",
    "K",
    "Q",
    "J",
    "10",
    "9",
    "8",
    "7",
    "6",
    "5",
    "4",
    "3",
    "2"
  ];
  highestValue = "";
  for (let i = 0; i < 12; i++) {
    for (let j = 0; j < 7; j++) {
      if (deck[j][1] === arrayHighestCard[i]) {
        highestCard.push(deck[j][0].concat(deck[j][1]));
        highestValue = deck[j][1]
      }
      if (highestCard.length === 1) {
        i = 13

      }
    }
  }
  if (highestCard.length === 1) {
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 7; j++) {
        //adiciona outras quatro cartas quaisquer
        if (highestCard.length <= 4 && highestValue !== deck[j][1]) {
          highestCard.push(deck[i][0].concat(deck[j][1]));

        }
      }
    }
  }
  if (highestCard.length === 5) {
    resultado = "highestCard";
    cartaParaComparacao = highestValue;
    deckFinal = highestCard
    return resultado, deckFinal, cartaParaComparacao;
  }

}

  function comparacao(comparacao) {
    const listaDeCombos = [
      "highestCard", "par", "doisPares", "trio", 
      "straight", "flush", "fullhouse", "quadra", 
      "straightFlush", "royalFlush"
    ]
    for (let i = 0; i < listaDeCombos.length; i++) {
        if (comparacao === listaDeCombos[i]) {
          valorDoCombo = i
          return valorDoCombo;
        }
      
    }
  }

  function desempateHighestCard(valorHighestCard){
    valorPadrao = [
      ["A"],["K"],["Q"],["J"],["10"],["9"],["8"],["7"],["6"],["5"],["4"],["3"],["2"]
    ]
    for (let i = 0; i < valorPadrao.length; i++) {
      if(valorHighestCard == valorPadrao[i]) {
        valorMaiorFinal = i
        return valorMaiorFinal;
      }
    }
  }

//inicialização das funções
createDeck();
shuffle(baralho);
cardDealer();


formatting(bot1, bot2);

beastValue(valores);
combos(valores);
bot1Combo = resultado
bot1DeckFinal = deckFinal
bot1highestCard = cartaParaComparacao

var qntCopas = 0;
var qntEspada = 0;
var qntOuro = 0;
var qntPaus = 0;

var royalFlush = [];
var straightFlush = [];
var quadra = [];
var cartaDeferente = true;
var fullHouse = [];
var fullHousebeckup
var flush = [];
var trio = []
var doisPares = [];
var segundoPar = [];
var par = [];
var highestCard = [];

beastValue(valores2);
combos(valores2);
bot2Combo = resultado
bot2DeckFinal = deckFinal
bot2highestCard = cartaParaComparacao

comparacao(bot1Combo);
var valorBot1 = valorDoCombo

valorDoCombo = 0;

comparacao(bot2Combo);
var valorBot2 = valorDoCombo

if(valorBot1 > valorBot2) {
  resultado = "Robô 1 venceu!"

} else if(valorBot2 > valorBot1) {
  resultado = "Robô 2 venceu!"

} else if(valorBot1 === valorBot2 && valorBot1 !== 0) {
  resultado = "Empate!"

} else if(valorBot1 === valorBot2 && valorBot1 === 0) {

  desempateHighestCard(bot1highestCard);
  var valorBot1 = valorMaiorFinal

  desempateHighestCard(bot2highestCard);
  var valorBot2 = valorMaiorFinal

  if(valorBot1 > valorBot2) {
    resultado = "Robô 1 venceu!"
  } else if(valorBot2 > valorBot1) {
    resultado = "Robô 2 venceu!"
  } else{
    resultado = "Empate!"
  }
  
}

console.log(`{ Robô 1: ${bot1Combo} (${bot1DeckFinal}),Robô 2: ${bot2Combo} (${bot2DeckFinal}), ${resultado} }`)